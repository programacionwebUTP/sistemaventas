document.addEventListener("DOMContentLoaded", function () {
	document.getElementById('asis').addEventListener('submit', validarDatos);
});

function validarDatos(asistencia){
	asistencia.preventDefault();

	let nom, ape, tecn, direc, telf, dist;
	nom = document.getElementById('nombre').value
	ape = document.getElementById('apellidos').value;
	direc = document.getElementById('direccion').value;
	telf = document.getElementById('celular').value;
	come = document.getElementById('comentario').value;
	// tecn = document.getElementById('tecnico').value;
	dist= document.getElementById('distrito').value;


	if(nom==""){
		alert("El nombre es un campo obligatorio a completar");
		return 0;
	}
	if(ape==""){
		alert("Los apellidos son un campo obligatorio a completar");
		return 0;
	}
	
	if(direc==""){
		alert("Debes colocar la dirección para proceder con la asistencia");
		return 0;
	}
	if(come==""){
		alert("Debes detallarnos mas sobre tu problema, para la asistencia");
		return 0;
	}
	if (dist=="Seleccionar") {
		alert("Debes seleccionar tu distrito")
		return 0;
	}
	// if(tecn=="Seleccionar"){
	// 	alert("Debes seleccionar el Tecnico");
	// 	return 0;
	// }
	if(telf==""){
		alert("El telefono es obligatorio a colocar como medio de contacto");
		return 0;
	}
	else if(isNaN(telf)){
		alert("El telefono debe ser numerico");
		return 0;
	
	}
		this.submit();
}
