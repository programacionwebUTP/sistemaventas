<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="css/miestilo.css">
	<link href="https://fonts.googleapis.com/css2?family=Rajdhani&display=swap" rel="stylesheet">
</head>
<body>
<?php

include("includes/header.php");
// session_start();
//     	if(isset($_SESSION['user'])){
		
//         	echo "<h1>Bienvenido: ".$_SESSION['user']."</h1>";
//         	echo '<div> <a href="llamadas/procesoUserCerrar.php" >Cerrar sesion</a></div>';
// 			echo '</div>';
// 			echo '</div>';
//         }

?>
	<img id="imagen-principal" src="imagenes/principal.png">
	<h3>Tema</h3>

	<input type="button" name="" value="Modo Oscuro" onclick= "fondoOscuro()">
	<input type="button" name="" value="Predeterminado" onclick= "fondoNormal()"><br>
	<section>
		<h2 class="centrado" id="Nosotros">Nosotros</h2>

		<div class="contenido-columnas">
			
			<img class="item-nosotros" src="imagenes/nosotros.png">
			<p class="item-nosotros">La Empresa SamSan Tech cuyo inicio de operaciones se realizó durante tiempos de pandemia. El giro de negocio es ventas minoristas de productos electrónicos, los cuales se podrán clasificar en seguridad, audio, video y telecomunicaciones en general.			
			</p>	
		</div>
	</section>

	<main class="centrado" id="MisVis">

		<h2>Mision y Vision</h2>

		<div class="contenido-columnas">

			<div class="mision-vision item-mivion">
				<img src="imagenes/mision.png">
				<p class="centrado">Ofrecer productos de calidad innovadores en tecnología				
				</p>				
			</div>	
			<div class="mision-vision item-mivion">
				<img src="imagenes/vision.png">
				<p>Posicionarnos como la empresa líder en el mercado.
				</p>
			</div>		
		</div>
	</main>


<br>
<?php

include("includes/footer.php");
?>
<center>
<a style="font-size: 30px" href="index.php">Regresar al inicio</a>
</center>	
</body>
</html>
