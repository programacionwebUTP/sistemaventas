-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 04-12-2022 a las 02:20:31
-- Versión del servidor: 10.4.25-MariaDB
-- Versión de PHP: 7.4.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `bdsamsantech`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `codcate` varchar(10) NOT NULL,
  `nomcate` varchar(50) NOT NULL,
  `descate` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`codcate`, `nomcate`, `descate`) VALUES
('C01', 'Portatiles', 'Intel'),
('C02', 'Equipos Gamer', 'Monitor Gamer');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `CodProd` varchar(10) NOT NULL,
  `NomProd` varchar(100) NOT NULL,
  `PreProd` double(10,2) NOT NULL,
  `FotoProd` varchar(100) NOT NULL,
  `codcate` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`CodProd`, `NomProd`, `PreProd`, `FotoProd`, `codcate`) VALUES
('P0001', 'LAPTOP LENOVO D330-10IGL, CELERON 4020 8GB 128GB SSD 10,1 W10', 1999.00, '../imagenes/intel1.webp', 'C01'),
('P0002', 'LAPTOP INTEL 7 256 GB 10ma GENERACION WINDOWS', 1997.00, '../imagenes/intel2.webp', 'C01');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicios`
--

CREATE TABLE `servicios` (
  `IDServicio` varchar(4) NOT NULL,
  `descripcion` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `servicios`
--

INSERT INTO `servicios` (`IDServicio`, `descripcion`) VALUES
('S001', 'Soporte Tecnico'),
('S002', 'Instalacion de sistemas operativos'),
('S003', 'Instalacion de programas de antivirus');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tecnicos`
--

CREATE TABLE `tecnicos` (
  `IDTecnico` varchar(4) NOT NULL,
  `nomtec` varchar(30) NOT NULL,
  `apetec` varchar(30) NOT NULL,
  `IDServicio` varchar(4) NOT NULL,
  `fototec` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `tecnicos`
--

INSERT INTO `tecnicos` (`IDTecnico`, `nomtec`, `apetec`, `IDServicio`, `fototec`) VALUES
('T001', 'Daniel', 'Bernaola', 'S001', 'imagen15.png'),
('T002', 'Cesar', 'Escalante', 'S002', 'imagen16.png'),
('T003', 'Gustavo', 'Cuba', 'S003', 'imagen17.png'),
('T004', 'Miranda', 'Molina', 'S004', 'imagen18.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `nomuser` varchar(10) NOT NULL,
  `passuser` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`nomuser`, `passuser`) VALUES
('user', '123');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `nomusu` varchar(10) NOT NULL,
  `pasusu` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`nomusu`, `pasusu`) VALUES
('sistemas', '1234'),
('utp', '1234');

--
-- Estructura de tabla para la tabla `detalleReclamo`
CREATE TABLE `detallereclamo` (
  `nombre` varchar(20) NOT NULL,
  `apellido` varchar(20) NOT NULL,
  `correo` varchar(20) NOT NULL,
  `Direccion` varchar(20) NOT NULL,
  `celular` varchar(9) NOT NULL,
  `comentario` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
--
--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`codcate`);

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`CodProd`),
  ADD KEY `codcate` (`codcate`);

--
-- Indices de la tabla `servicios`
--
ALTER TABLE `servicios`
  ADD PRIMARY KEY (`IDServicio`);

--
-- Indices de la tabla `tecnicos`
--
ALTER TABLE `tecnicos`
  ADD PRIMARY KEY (`IDTecnico`),
  ADD KEY `IDServicio` (`IDServicio`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`nomuser`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`nomusu`);

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `productos`
--
ALTER TABLE `productos`
  ADD CONSTRAINT `productos_ibfk_1` FOREIGN KEY (`codcate`) REFERENCES `categoria` (`codcate`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
