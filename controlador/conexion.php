<?php
 //variable conexion
function conectar() {
    $conn= mysqli_connect ("localhost","root","","bdsamsantech"); 
/*  if(!$conn){
        die("No puede conectarse ".mysqli_error());
    }
    else{
        echo "Conexión satisfactoria";
    }*/
    return $conn; 
}
function validarLogueo($usu,$pas,$conn){
    debug_to_console([$usu , $pas ]);
    $sql="select * from usuario where nomusu='$usu' and pasusu='$pas'";
    $res= mysqli_query($conn, $sql);
    $filas = mysqli_num_rows($res);
    return $filas; 
}

//método para agregar registros
function agregarProducto($cod,$nom,$pre,$fot,$cat,$conn){
    $sql="insert into Productos values('$cod','$nom','$pre','$fot','$cat')";   
    mysqli_query($conn, $sql) or die(mysqli_error($conn));
}

 //método para eliminar registros
function eliminarProducto($cod,$conn){
    $sql="delete from Productos where CodProd='$cod'";    
    mysqli_query($conn, $sql) or die(mysqli_error($conn));
}

 //método para actualizar todos los campos de la tabla
function actualizarTodoProducto($cod,$nom,$pre,$fot,$cat,$conn){
    $sql="update Productos set NomProd='$nom', PreProd='$pre', FotoProd='$fot', codcate='$cat' where CodProd='$cod'";   
    mysqli_query($conn, $sql) or die(mysqli_error($conn));
}

 //método para actualizar registros sin foto
function actualizarProducto($cod,$nom,$pre,$cat,$conn){
    $sql="update Productos set NomProd='$nom', PreProd='$pre', codcate='$cat' where CodProd='$cod'"; 
    mysqli_query($conn, $sql) or die(mysqli_error($conn));
}

 //método para buscar registro
function buscarProducto($cod,$conn){
    $sql="select NomProd, PreProd, FotoProd, codcate from Productos where CodProd='$cod'";
    $res= mysqli_query($conn, $sql);
    $vec=array();
    if(mysqli_num_rows($res)>0){
        $vec= mysqli_fetch_array($res);
    }
    return $vec; 
}
// function validarLogueo($usu,$pas,$conn){
//     $sql="select * from usuario where nomusu='$usu' and pasusu='$pas'";
//     $res= mysqli_query($conn, $sql);
//     $filas = mysqli_num_rows($res);
//     return $filas; 
// }
    
//método para listar registros
function listarProducto($conn){
    $sql="select CodProd, NomProd, PreProd, FotoProd, codcate, stock from Productos"; 
    $res= mysqli_query($conn, $sql);
    $vec=array();
    while($f= mysqli_fetch_array($res))  
        $vec[]=$f;
    return $vec;
}
function Createproducto($codprod,$nomprod,$precprod,$fotoprod,$codcate,$stock,$conn){
    $sql="insert into productos values('$codprod','$nomprod','$precprod','$fotoprod','$codcate','$stock')";   
    mysqli_query($conn, $sql) or die(mysqli_error($conn));
}
 
function Deleteproducto($codprod,$conn){
    $sql="delete from productos where codprod='$codprod'";    
     mysqli_query($conn, $sql) or die(mysqli_error($conn));
}


//Tabla categoria 
//método para agreegar registros
function agregarCategoria($cod,$nom,$desc,$conn){
    $sql="insert into categoria values('$cod','$nom','$desc' )";   
    mysqli_query($conn, $sql) or die(mysqli_error($conn));
}

//método para eliminar registros 
function eliminarCategoria($cod,$conn){
    $sql="delete from categoria where codcate='$cod'";   
    mysqli_query($conn, $sql) or die(mysqli_error($conn));
}

//método para actualizar registros sin foto
function actualizarCategoria($cod,$nom,$desc,$conn){
    $sql="update categoria set nomcate='$nom', descate='$desc' where codcate='$cod'"; 
    mysqli_query($conn, $sql) or die(mysqli_error($conn));
}

//método para buscar registro
function buscarCategoria($cod,$conn){
    $sql="select nomcate, descate from categoria where codcate='$cod'";
    $res= mysqli_query($conn, $sql);
    $vec=array();
    if(mysqli_num_rows($res)>0){
        $vec= mysqli_fetch_array($res);
    }
    return $vec; 
}

//método para listar registros
function listarCategoria($conn){
    $sql="select codcate, nomcate, descate from categoria";
    $res= mysqli_query($conn, $sql);
    $vec=array();
    while($f= mysqli_fetch_array($res))  
        $vec[]=$f;
    return $vec;

 }

 //método para agreegar registros
function agregarServicio($cod,$nom,$conn){
    $sql="insert into servicios values('$cod','$nom')";   
    mysqli_query($conn, $sql) or die(mysqli_error($conn));
}
//método para eliminar servicios 
function eliminarServicio($cod,$conn){
    $sql="delete from servicios where IDServicio='$cod'";    
    mysqli_query($conn, $sql) or die(mysqli_error($conn));
}

function listarServicio($conn){
    $sql="select IDServicio, descripcion from servicios";
    $res= mysqli_query($conn, $sql);
    $vec=array();
    while($f= mysqli_fetch_array($res))  
        $vec[]=$f;
    return $vec;
}
function buscarServicio($cod,$conn){
    $sql="select descripcion from servicios where IDServicio='$cod'";
    $res= mysqli_query($conn, $sql);
    $vec=array();
    if(mysqli_num_rows($res)>0){
        $vec= mysqli_fetch_array($res);
    }
    return $vec; 
 }
 //método para actualizar registros 
function actualizarServicio($cod,$nom,$conn){
    $sql="update servicios set descripcion='$nom' where IDServicio='$cod'"; 
    mysqli_query($conn, $sql) or die(mysqli_error($conn));
}
//método para agreegar tecnicos
function agregarTecnicos($cod,$nom,$ape,$cser,$fot,$conn){
    $sql="insert into tecnicos values('$cod','$nom','$ape','$cser','$fot')";   
    mysqli_query($conn, $sql) or die(mysqli_error($conn));
}
//metodo para listar tecnicos
function listarTecnico($conn){
    $sql="select IDTecnico, nomtec, apetec, IDServicio, fototec from tecnicos";
    $res= mysqli_query($conn, $sql);
    $vec=array();
    while($f= mysqli_fetch_array($res))  
        $vec[]=$f;
    return $vec;
}
//método para eliminar tecnicos 
function eliminarTecnico($cod,$conn){
    $sql="delete from tecnicos where IDTecnico='$cod'";   
    mysqli_query($conn, $sql) or die(mysqli_error($conn));
}
 //método para buscar tecnico
function buscarTecnico($cod,$conn){
    $sql="select nomtec, apetec, IDServicio,fototec from tecnicos where IDTecnico='$cod'";
    $res= mysqli_query($conn, $sql);
    $vec=array();
    if(mysqli_num_rows($res)>0){
        $vec= mysqli_fetch_array($res);
    }
    return $vec; 
}
function actualizarTecnico($cod,$nom,$ape,$codserv,$fot,$conn){
    $sql="update tecnicos set nomtec='$nom', apetec='$ape', IDServicio='$codserv',fototec='$fot' where IDTecnico='$cod'"; 
    mysqli_query($conn, $sql) or die(mysqli_error($conn));
}



function debug_to_console($data) {
    $output = $data;
    if (is_array($output))
        $output = implode(',', $output);

    echo "<script>console.log('Debug Objects: " . $output . "' );</script>";
}

//metodos para usuarios
//método para agreegar registros
function agregarUsers($nom,$pass,$nomComp,$conn){
    debug_to_console([$nom , $pass ]);
    $sql="insert into users values('$nom','$pass', '$nomComp')";   
    mysqli_query($conn, $sql) or die(mysqli_error($conn));
}
//método para eliminar servicios 
function eliminarUsuario($cod,$conn){
    $sql="delete from users where nomuser='$cod'";    
    mysqli_query($conn, $sql) or die(mysqli_error($conn));
}

function listarUsuario($conn){
    $sql="select nomuser, passuser , nombreCompleto from users";
    $res= mysqli_query($conn, $sql);
    $vec=array();
    while($f= mysqli_fetch_array($res))  
        $vec[]=$f;
    return $vec;
}
function actualizarUser($nom,$passwd,$nomCom,$conn){
    debug_to_console([$nom , $passwd ,$nomCom ]);
    $sql="update users set nomuser='$nom', passuser='$passwd', nombreCompleto='$nomCom' where nomuser='$nom'"; 
    mysqli_query($conn, $sql) or die(mysqli_error($conn));
}

//funcion login cliente
function validarUser($conn,$user,$clave){
    debug_to_console([$user , $clave ]);
    $sql="select * from users where nomuser='$user' and passuser='$clave'";
    $resu=mysqli_query($conn,$sql);
    //$nFilas=mysqli_num_rows($resu);
    return $resu;
}

function buscarUsuario($cod,$conn){
    $sql="select nomuser, passuser , nombreCompleto	 from users where nomuser='$cod'";
    $res= mysqli_query($conn, $sql);
    $vec=array();
    if(mysqli_num_rows($res)>0){
        $vec= mysqli_fetch_array($res);
    }
    return $vec; 
}
//RECLAMO
function agregarReclamo($nom,$ape,$correo,$direc,$cel,$coment,$conn){
    

    $sql="insert into detallereclamo values('$nom','$ape','$correo','$direc','$cel','$coment')";   
    mysqli_query($conn, $sql) or die(mysqli_error($conn));
    
}
function listarReclamo($conn){
    $sql="select * from detallereclamo";
    $res= mysqli_query($conn, $sql);
    $vec=array();
    while($f= mysqli_fetch_array($res))  
        $vec[]=$f;
    return $vec;
}