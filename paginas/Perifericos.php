<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="../css/Productos.css">
	<div class="conte_parent" id="cont_parent"></div>
</head>

<fieldset class="centrado">AUDIFONOS</fieldset>

		<div class="conte">
			<div class="card equipos">
				<img src="../imagenes/aud1.jpg" width="300" >
			</div>

			<div class="info">
				<p>Audífonos Radioshack con micrófono, conexión usb, sonido estéreo, control de volumen, negro</p>
			</div>

			<div class="CPrecio">
				<div class="precio">
					<span class="precio1"><b>S./49.90</b></span>	
				</div>
			</div>	

		</div>


		<div class="conte">
			<div class="card equipos">
				<img src="../imagenes/aud2.jpg" width="300" >
			</div>

			<div class="info">
				<p>Audífono Radioshack con micrófono, conexión usb, cable 2 metros, negro</p>
			</div>

			<div class="CPrecio">
				<div class="precio">
					<span class="precio1"><b>S./49.90</b></span>	
				</div>
			</div>		
		</div>

		<div class="conte">
			<div class="card equipos">
				<img src="../imagenes/kit1.jpg" width="300" >
			</div>

			<div class="info">
				<p>Kit inalámbrico Teraware teclado y mouse, membrana, receptor usb, usa pilas, negro</p>
			</div>

			<div class="CPrecio">
				<div class="precio">
					<span class="precio1"><b>S./79.90</b></span>	
				</div>
			</div>		
		</div>

<fieldset class="centrado">KIT TECLADO Y MOUSE </fieldset>

		<div class="conte">
			<div class="card equipos">
				<img src="../imagenes/kit2.jpg" width="300" >
			</div>

			<div class="info">
				<p>Kit teclado y mouse inalámbrico Logitech MK235, membrana, receptor usb, usa pilas, negro</p>
			</div>
			<div class="CPrecio">
				<div class="precio">
				<center>
					<span class="precio1"><b>S./94.90</b></span>	
					</center>
				</div>
			</div>		
		</div>

<fieldset class="centrado"> ESTABILIZADORES DE ENERGIA PARA PC/LAPTOPS </fieldset>

		<div class="conte">
			<div class="card equipos">
				<img src="../imagenes/est1.jpg" width="100" >
			</div>

			<div class="info">
				<p>Estabilizador de corriente Forza Power Technologies FVR-2202 8 tomas, 2200VA/1100W"</p>
			</div>

			<div class="CPrecio">
				<div class="precio">
					<span class="precio1"><b>S./169.00</b></span>	
				</div>
			</div>		
		</div>

		<div class="conte">
			<div class="card equipos">
				<img src="../imagenes/est2.jpg" width="300" >
			</div>
				
			<div class="info">
				<p>Estabilizador FORZA FVR-1002 1000VA/500W</p>
			</div>
			
			<div class="CPrecio">
				<div class="precio">
					<span class="precio1"><b>S./63.00</b></span>
				</div>
			</div>		
		</div>



<fieldset class="centrado">TECLADOS</fieldset>

		<div class="conte">
			<div class="card equipos">
				<img src="../imagenes/tec1.jpg" width="300" >
			</div>
				<div class="info">
					<p>Teclado Genius Luxemate 100</p>
				</div>
					<div class="CPrecio">
						<div class="precio">
							<span class="precio1"><b>S./42.90</b></span>	
						</div>
					</div>		
		</div>

		<div class="conte">
			<div class="card equipos">
				<img src="../imagenes/tec2.jpg" width="300">
			</div>
				<div class="info">
					<p>Teclado Microsoft QSZ-00003</p>
				</div>
					<div class="CPrecio">
						<div class="precio">
							<span class="precio1"><b>S./189.00</b></span>
						</div>
					</div>		
		</div>

		<div class="conte">
			<div class="card equipos">
				<img src="../imagenes/mou1.jpg" width="300" >
			</div>
				
			<div class="info">
				<p>Mouse inalámbrico Teraware, receptor usb, 1600 dpi, 3 botones, usa pilas, negro</p>
			</div>
			
			<div class="CPrecio">
				<div class="precio">
					<span class="precio1"><b>S./29.90</b></span>	
				</div>
			</div>		
		</div>

		<div class="conte">

			<div class="card equipos">
				<img src="../imagenes/mou2.jpg" width="300" >
			</div>

			<div class="info">
				<p>Mouse inalámbrico Logitech Pebble M350 bluetooth, 1000 dpi, 3 botones, usa pila, rosado</p>
			</div>
	
			<div class="CPrecio">
				<div class="precio">
					<span class="precio1"><b>S./89.90</b></span>	
				</div>
			</div>

</div><fieldset class="centrado">MOUSE INALAMBRICOS</fieldset>

		<div class="conte">
			<div class="card equipos">
				<img src="../imagenes/par1.jpg" width="300">
			</div>

			<div class="info">
				<p>PARLANTE GENIUS SP-Q160 IRON GREY USB POWER 6W RMS</p>
			</div>

			<div class="CPrecio">
				<div class="precio">
					<span class="precio1"><b>S./35.37</b></span>
				</div>
			</div>		
		</div>

<fieldset class="centrado">  PARLANTES DESKTOP </fieldset>

		<div class="conte">
			<div class="card equipos">
				<img src="../imagenes/par2.jpg" width="300" >
			</div>

			<div class="info">
				<p>PARLANTE LOGITECH S150 DIGITAL STEREO BLACK USB</p>
			</div>

			<div class="CPrecio">
				<div class="precio">
					<span class="precio1"><b>S./45.20</b></span>	
				</div>
			</div>	
				
		</div>


<br>
<center>
<a style="font-size: 30px" href="Productos.php">REGRESAR A PRODUCTOS</a>
</center>
</body>
</html>