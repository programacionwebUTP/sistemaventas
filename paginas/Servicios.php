<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="../css/miestilo.css">
	<link href="https://fonts.googleapis.com/css2?family=Rajdhani&display=swap" rel="stylesheet">
</head>
<body>
<?php

include("../includes/header.php");
?>
		<main class="centrado">

		<h2>Nuestros Servicios</h2>

		

		<div class="contenido-columnas">

			<div class="foto-servicios item-servicios">
				<img src="../imagenes/imagen11.png">
				<p class="centrado">Soporte Tecnico			
				</p>				
			</div>	
			<div class="foto-servicios item-servicios">
				<img src="../imagenes/imagen12.png">
				<p>Instalacion de Sistemas Operativos
				</p>
			</div>			
		</div>
		<div class="contenido-columnas">

			<div class="foto-servicios item-servicios">
				<img src="../imagenes/imagen14.png">
				<p>Instalacion de programas de antivirus
				</p>
			</div>	
			<div class="foto-servicios item-servicios">
				<img src="../imagenes/imagen13.png">
				<p>Asesoria y armado de PC
				</p>
			</div>		
		</div>
	</main>

    <?php

include("../includes/footer.php");
?>

</body>
</html>