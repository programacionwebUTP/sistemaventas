<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="../../css/miestilo.css">
	<link href="https://fonts.googleapis.com/css2?family=Rajdhani&display=swap" rel="stylesheet">
</head>
<body>
<?php

include("../../includes/header.php");

// session_start();
		if(!isset($_SESSION['usuario']))
			header("location:../LoginAdmin.php");

		require('../../controlador/conexion.php');
		$conn=conectar();

?>
<h2>Listar reclamos</h2>

<div >
	<table>
		<tr>
			
            <th>Nombres</th>
            <th>Apellidos</th>
            <th>Correo electronico</th>
            <th>Direccion</th>
            <th>Celular</th>
            <th>Comentarios</th>
		</tr>
			<?php
				foreach (listarReclamo($conn) as $key => $value) {
			?>
			<tr>
				<td><?=$value[0]?></td>
				<td><?=$value[1]?></td>
				<td><?=$value[2]?></td>
                <td><?=$value[3]?></td>
                <td><?=$value[4]?></td>
                <td><?=$value[5]?></td>
			</tr>
			<?php
				}
				?>
		</table>		
</div>






<?php

include("../../includes/footer.php");
?>
</body>
</html>