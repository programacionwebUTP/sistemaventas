<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="../../css/miestilo.css">
	<link href="https://fonts.googleapis.com/css2?family=Rajdhani&display=swap" rel="stylesheet">
</head>
<body>

	<?php
session_start();
if(!isset($_SESSION['usuario']))
	header("location:../LoginAdmin.php");
	
		require('../../controlador/conexion.php');
		$conn = conectar();
	?>
	<h2>Listar Producto</h2>
	<div>
	<table>
		<tr>
			<th>Código</th>
			<th>Nombre</th>
			<th>Precio</th>
			<th>Foto</th>
			<th>Categoría</th>
			<th>Acción</th>
		</tr>
		<?php
			foreach (listarProducto($conn) as $key => $value) {
		?>
				<tr>
					<td><?=$value[0]?></td>
					<td><?=$value[1]?></td>
					<td>S/.<?=$value[2]?></td>
					<td><img src="../<?=$value[3]?>" width="100" height="120"></td>
					<td><?=$value[4]?></td>
					<td>
						<a href="../../llamadas/procesoProducto.php?accion=eliminar&codigo=<?=$value[0]?>">Eliminar</a>
						<a href="editar.php?codigo=<?=$value[0]?>">Modificar</a>
					</td>
				</tr>
		<?php
			}
		?>

	</table>
	</div>