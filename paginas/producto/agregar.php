<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<?php

		require('../../controlador/conexion.php');
		$conn=conectar();
	?>
	<h2>Agregar Producto</h2>
	<form action="../../llamadas/procesoProducto.php" method="post" enctype="multipart/form-data">
			<label>Codigo Producto</label>
			<input  type="text" name="codigo"><br>
			<label>Nombre Producto</label>
			<input  type="text" name="nombre"><br>
			<label>Precio Producto</label>
			<input  type="number" name="precio"><br>
			<label>Foto Producto</label>
			<input  type="file" name="foto"><br>
			<label>Categoría</label>
			<select  name="categoria" value="<?=$data[4]?>">
				<?php
				foreach (listarCategoria($conn) as $key => $value) {
				?>
					<option value="<?=$value[0]?>"><?=$value[0]?></option>
				<?php
					}
				?>
			</select>
			<label>Stock</label>
			<input  type="number" name="stock"><br>
			<input type="hidden" name="accion" value="agregar">
			<input type="submit" name="aceptar" value="Aceptar">
	</form>	
</body>
</html>