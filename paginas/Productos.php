<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="../css/miestilo.css">
	<link href="https://fonts.googleapis.com/css2?family=Rajdhani&display=swap" rel="stylesheet">
</head>
<body>
<?php

include("../includes/header.php");
?>

<fieldset class="centrado">NUESTROS PRODUCTOS</fieldset>
<div class="container">
	<div class="prod prod1">
		<div>
			<h3 class="tit">Portatiles</h3>
		</div>
		<div class="immg">
			<a href="Portatiles.php"><img src="../imagenes/imagen4.png" width="300"></a>
		</div>
			<div class="infor">
				<li>APPLE</li>
				<li>DELL</li>
				<li>LENOVO</li>
			</div>
	</div>

<div class="prod prod2">
	<h3 class="tit">Equipos Gamer</h3>
	<div class="immg">
		<a href="EquiposGamer.php"><img src="../imagenes/imagen5.png" width="270" ></a>
	</div>
		<div class="infor">
			<li>Portatil Gamer</li>
			<li>Teclado gamer</li>
			<li>Mouse gamer</li>
			<li>audifonos gamer</li>
		</div>
</div>

<div class="prod prod3">
	<h3 class="tit">Audiovisuales</h3>
	<div class="immg">
		<a href="Audiovisuales.php"><img src="../imagenes/imagen6.png" width="240" height="180"></a>
	</div>
		<div class="infor">
			<li>Camara profesional</li>
			<li>Parlantes</li>
			<li>Camara Instantanea</li>
			<li>Tablets</li>
			<li>Celulares</li>
		</div>
</div>


<div class="prod prod4">
	<h3 class="tit">Seguridad</h3>
	<div class="immg">
		<a href="Seguridad.php"><img src="../imagenes/imagen7.png" width="300" height="190" ></a>
	</div>
		<div class="infor">
			<li>Camara vigilancia</li>
			<li>Cerradura electronica</li>
			<li>Drones</li>
		</div>
</div>

<div class="prod prod5">
	<h3 class="tit">Suministros</h3>
	<div class="immg">
		<a href="Suministros.php"><img src="../imagenes/imagen8.png" width="340"></a>
	</div>
		<div class="infor">
			<li>Tintas</li>
			<li>Toners</li>
		</div>
</div>

<div class="prod prod6">
	<h3 class="tit">Perifericos</h3>
	<div class="immg">
		<a href="Perifericos.php"><img src="../imagenes/imagen9.png" width="350"></a>
	</div>
		<div class="infor">
			<li>Audifonos</li>
			<li>Kit Teclado - Mouse</li>
			<li>Estabilizadores de energia</li>
			<li>Teclado</li>
			<li>Mouse</li>
			<li>Parlantes</li>
		</div>
</div>

<div class="prod prod7">
	<h3 class="tit">Accesorios</h3>
	<div class="immg">
		<a href="Accesorios.php"><img src="../imagenes/imagen10.png" width="300"></a>
	</div>
		<div class="infor">
			<li>Camara web</li>
			<li>Almacenamiento</li>
			<li>Memoria Ram</li>
			<li>Procesador</li>
		</div>
</div>
</div>
<br>
<?php

include("../includes/footer.php");
?>
<center>
<a style="font-size: 30px;" href="http://localhost/sistemaventas-main/index.php">Regresar a la página principal</a>
</center>
</body>
</html>