<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="../../css/miestilo.css">
	<link href="https://fonts.googleapis.com/css2?family=Rajdhani&display=swap" rel="stylesheet">
	</head>
<body>



	<?php

	session_start();
			if(!isset($_SESSION['usuario']))
				header("location:../LoginAdmin.php");


		require '../../controlador/conexion.php';
		$conn = conectar();
	?>

	<h2>Listar Categoria</h2>
	<div>
	<table >
		<tr>
			<th>Codigo</th>
			<th>Nombre</th>
			<th>Descripción</th>
			<th>Acción</th>
		</tr>
		<?php
			foreach (listarCategoria($conn) as $key => $value) {
		?>
			<tr>
				<td><?=$value[0]?></td>
				<td><?=$value[1]?></td>
				<td><?=$value[2]?></td>
				<td>
					<a href="../../llamadas/procesoCategoria.php?accion=eliminar&codigo=<?=$value[0]?>">Eliminar</a>
					<a href="editar.php?accion=eliminar&codigo=<?=$value[0]?>">Modificar</a>
				</td>
			</tr>
		<?php
			}
		?>
		
	</table>


</body>
</html>