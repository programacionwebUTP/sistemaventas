<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="../../css/miestilo.css">
	<link href="https://fonts.googleapis.com/css2?family=Rajdhani&display=swap" rel="stylesheet">
</head>
<body>

	<?php

		require('../../controlador/conexion.php');
		$conn = conectar();
		$cod = $_REQUEST['codigo'];
		$data = buscarCategoria($cod,$conn)
	?>
	<h2>Editar Categoria</h2>
	<form action="../../llamadas/procesoCategoria.php" method="post">
			
			<input type="hidden" name="codigo" value="<?=$cod?>"><br>
			<label>Nombre</label>
			<input type="text"  name="nombre" value="<?=$data[0]?>"><br>
			<label>Descripcion</label>
			<input type="text"  name="descripcion"  value="<?=$data[1]?>">
			
			<input type="hidden" name="accion" value="modificar">
			<input type="submit" name="actualizar" value="Actualizar">
	</form>	
	


</body>
</html>