<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="../css/miestilo.css">
	<link href="https://fonts.googleapis.com/css2?family=Rajdhani&display=swap" rel="stylesheet">
    <script type="text/javascript" src="../js/entregable4.js"></script>
</head>
<body>
	<?php
include("../includes/header.php");
?>



<?php
		if($_POST){
			if(isset($_POST['ejercicio1'])){
				$cuota;
				$p = $_POST['montoCompra']; 
				$tipo = $_POST['tipoTarjeta']; 
				$n = $_POST['cantMeses']; 
				$porcent = ($tipo/100); 
	
				$cuota = ($p*$porcent)/(1-pow(1+($porcent),(-$n)));
	
				 $deu=$p;
				number_format($deu,2);
				$cad='<table border="1"><tr><th>Mes</th><th>Pago mensual</th><th>Pago del interés</th><th>Pago del capital</th><th>Deuda</th></tr>';
				
				for($i = 1; $i <= $n; $i++){
					$int = $deu*$porcent; 
					$cap = $cuota - $int;
					$deu -= $cap;
	
					$cad .= '<tr>';
					$cad .= '<td>' . $i . '</td>';
					$cad .= '<td>' . number_format($cuota,2) . '</td>';
					$cad .= '<td>' . number_format($int,2) . '</td>';
					$cad .= '<td>' . number_format($cap,2) . '</td>';
					$cad .= '<td>' . number_format($deu,2) . '</td>';
					$cad .= '</tr>';
	
				}
				$cad .= '</table>';
			}
	
	
	
	
			if(isset($_POST['ejercicio2'])){
                $numero = $_POST['cantNumeros']; 
				$tab2='<table border="1"><tr><th>N°</th><th>Número</th><th>Suma de dígitos</th><th>Descripción</th></tr>';
				
				for($i = 1; $i <= $numero; $i++){

                    $numero_aleatorio = mt_rand(20,100);

                    $suma = sumaDigitos(strval($numero_aleatorio));

                    $tipo =  ($suma % 2) == 0 ? 'par' : 'impar';

	
					$tab2 .= '<tr>';
					$tab2 .= '<td>' . $i . '</td>';
					$tab2 .= '<td>' . $numero_aleatorio . '</td>';
					$tab2 .= '<td>' . $suma . '</td>';
					$tab2 .= '<td>' . $tipo . '</td>';					
					$tab2 .= '</tr>';
	
				}
				$tab2 .= '</table>';

			}
	
			if(isset($_POST['ejercicio3'])){

				$nomCli = $_POST['nombreCli']; 
				$direCli = $_POST['direccionCli']; 
				$nomPro = $_POST['nombreProd']; 
                $tipPro = $_POST['tipoProd']; 
                $preUn = $_POST['precioUni']; 
                $canPro = $_POST['cantProd']; 
                $desc;
                $sub;
                $total;

                switch ($tipPro) {
                    case 'abarrotes':
                       if($canPro<=6){
                        $desc = (5/100);
                       }else{
                        $desc = (8/100);
                       }
                        break;
                        case 'limpieza':
                            if($canPro<=6){
                                $desc = (7/100);
                               }else{
                                $desc = (10/100);
                               }
                            break;
                            case 'juguetes':
                                if($canPro<=6){
                                    $desc = (10/100);
                                   }else{
                                    $desc = (13/100);
                                   }
                                break;
                                case 'otros':
                                    if($canPro<=6){
                                        $desc = (12/100);
                                       }else{
                                        $desc = (15/100);
                                       }
                                    break;                    
                    
                    default:
                        # code...
                        break;
                }

                $sub = $preUn * $canPro;
                $descuento = $sub * $desc;
                $total = $sub - $descuento;



			}
	
	
	
	
			if(isset($_POST['ejercicio4'])){
				$cont=0; /*cont acumulador*/
				$cont2=0; /*cont2 acumulador2 ,*/
				/*i es el numero que se va probar que es primo, 
				j es el divisor*/
				for($i=2;$cont2<$_POST['numero']; $i++)
				{
					for($j=$i;$j>0;$j--)
						if($i%$j==0)
							$cont++;
	
						if($cont<=2){
							$cont2++;
							echo $i.", ";
						}
						$cont=0;
	
				}
			}
		}




        function sumaDigitos($num) {
            $sum = 0;
            for ($i = 0; $i < strlen($num); $i++){
                $sum += $num[$i];
            }
            return $sum;
        }
		
?>


<!-- ejercicio 1 -->
<main>
<div>
<form method="post"  onsubmit="return ejercicio1();">

    <fieldset class="centrado">
        <legend>Ejercicio 1</legend> 
        <label>Ingresa lel monto de la compra</label>
        <input type="text" name="montoCompra" value="<?php if(isset($p)) echo $p; ?>"><br><br>
        <label>Ingresa tipo de tarjeta</label><br>
        <select name="tipoTarjeta" id="">
            <option value="2" <?php if(isset($tipo)) if ($tipo === '2') echo ' selected="selected"'; ?>>Visa</option>
            <option value="2.5" <?php if(isset($tipo)) if ($tipo === '2.5') echo ' selected="selected"'; ?>>Master Card</option>
            <option value="3" <?php if(isset($tipo)) if ($tipo === '3') echo ' selected="selected"'; ?>>American Express</option>
        </select> <br> <br>
        <label>Ingresa la cantidad de meses a fraccionar</label>
        <input type="text" name="cantMeses" value="<?php if(isset($n)) echo $n; ?>"><br><br>
        <input type="submit" name="ejercicio1" onclick="ejercicio1()" value="Calcular"><br><br>
        <?php if(isset($cuota)) echo 'Pago mensual: '.number_format($cuota,2); ?>
        <br>
            <center>
				<div id="cont" style="margin-left: auto; margin-right: auto;">
				<?php if(isset($cad)) echo $cad; ?>
				</div>
			</center>

    </fieldset>

</form>
</div>
</main>   
<hr>

<!-- ejercicio 2 -->

<form method="post"  >

    <fieldset class="centrado">
        <legend>Ejercicio 2</legend> 
        <label>Ingresa la cantidad de numero</label>
        <input type="text" name="cantNumeros" value="<?php if(isset($numero)) echo $numero; ?>"><br><br>
        <input type="submit" name="ejercicio2" value="Buscar"><br><br>
        <center>
				<div id="cont" style="margin-left: auto; margin-right: auto;">
				<?php if(isset($tab2)) echo $tab2; ?>
				</div>
			</center>
    </fieldset>

</form>
<hr>

<!-- ejercicio 3 -->

<form method="post"  >

    <fieldset class="centrado">
        <legend>Ejercicio 3</legend> 
        <label>Ingresa nombre</label><br>
        <input type="text" name="nombreCli" value="<?php if(isset($nomCli)) echo $nomCli; ?>"><br><br>
        <label>Ingresar dirección</label><br>
        <input type="text" name="direccionCli" value="<?php if(isset($direCli)) echo $direCli; ?>"><br><br>
        <label>Ingrese nombre del producto</label><br>
        <input type="text" name="nombreProd" value="<?php if(isset($nomPro)) echo $nomPro; ?>"><br><br>
        <label>Seleccione el tipo de producto</label><br>
       <select name="tipoProd" id="" >
        <option value="abarrotes" <?php if(isset($tipPro)) if ($tipPro === 'abarrotes') echo ' selected="selected"'; ?>>Abarrotes</option>
        <option value="limpieza" <?php if(isset($tipPro)) if ($tipPro === 'limpieza') echo ' selected="selected"'; ?>>Limpieza</option>
        <option value="juguetes" <?php if(isset($tipPro)) if ($tipPro === 'juguetes') echo ' selected="selected"'; ?>>Juguetes</option>
        <option value="otros" <?php if(isset($tipPro)) if ($tipPro === 'otros') echo ' selected="selected"'; ?>>Otros</option>
       </select><br><br>
       <label>Ingrese precio unitario</label><br>
        <input type="text" name="precioUni" value="<?php if(isset($preUn)) echo $preUn; ?>"><br><br>
        <label>Ingrese cantidad</label><br>
        <input type="text" name="cantProd" value="<?php if(isset($canPro)) echo $canPro; ?>"><br><br>

        <input type="submit" name="ejercicio3" value="Buscar"><br><br>

        <div id="cont" style="margin-left: auto; margin-right: auto;">
				<?php 
                if(isset($total)) {
                
                 echo 'Subtotal: '.$sub.'<br>';
                 echo 'Descuento: '.$descuento.'<br>';
                 echo 'Total: '.$total.'<br>';
                }
                  ?>
		</div>
    </fieldset>

</form>

<hr>

<!-- ejercicio 4 -->
<form method="post" action="salidaEntregable4.php" target="result" >

    <fieldset class="centrado">
        <legend>Ejercicio 4</legend> 
        <label>Ingresa la cantidad de numero</label>
        <input type="text" name="numero"><br><br>
        <input type="submit" name="ejercicio4" value="Buscar4"><br><br>
        <iframe src="" name="result"  ></iframe><br><br>
        <!-- <div style="text-align: end;" id="result4"></div> -->
    </fieldset>

</form>


<?php
		
include("../includes/footer.php");
?>
</body>
</html>