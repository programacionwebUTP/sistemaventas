<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="../../css/miestilo.css">
	<link href="https://fonts.googleapis.com/css2?family=Rajdhani&display=swap" rel="stylesheet">
</head>

<body>
	<?php
		
		require('../../controlador/conexion.php');
		$conn = conectar();
		$cod = $_REQUEST['codigo'];
		$data = buscarServicio($cod,$conn);
	?>
	<h1>Editar Servicio</h1>
	<form action="../../llamadas/procesoServicio.php" method="post">
		<input type="hidden" name="codigo" value="<?=$cod?>"><br>
		<label>Nombre</label>
		<input type="text" name="nombre" value="<?=$data[0]?>"><br>
		<input type="hidden" name="accion" value="modificar">
		<input type="submit" name="actualizar" value="Actualizar">
	</form>
</body>

</html>