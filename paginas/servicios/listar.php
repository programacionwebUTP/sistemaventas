<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="../../css/miestilo.css">
	<link href="https://fonts.googleapis.com/css2?family=Rajdhani&display=swap" rel="stylesheet">

</head>
<body>
<?php

session_start();
			if(!isset($_SESSION['usuario']))
				header("location:../LoginAdmin.php");


		require('../../controlador/conexion.php');
		$conn=conectar();
	?>
<h1>Listar Servicio</h1>
<div >
	<table>
		<tr>
			<th >Codigo</th>
			<th>Descripcion</th>
			<th>Accion</th>
		</tr>
			<?php
				foreach (listarServicio($conn) as $key => $value) {
			?>
			<tr>
				<td><?=$value[0]?></td>
				<td><?=$value[1]?></td>
				<td>
					<nav class="navegacion">
						<a href="../../llamadas/procesoServicio.php?accion=eliminar&codigo=<?=$value[0]?>">Eliminar</a>
						<a href="editar.php?codigo=<?=$value[0]?>">Modificar</a>
					</nav>
				</td>
			</tr>
			<?php
				}
				?>
		</table>		
</div>		
</body>
	
</html>