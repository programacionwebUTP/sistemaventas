<!DOCTYPE html>
<html>
<head>
	<title></title>
<link rel="stylesheet" type="text/css" href="../../css/miestilo.css">
	<link href="https://fonts.googleapis.com/css2?family=Rajdhani&display=swap" rel="stylesheet">
	
</head>

<body>
	<?php

		require('../../controlador/conexion.php');
		$conn=conectar();
	?>
	<h1>Agregar Tecnicos</h1>
		<form action="../../llamadas/procesoTecnico.php" method="post" enctype="multipart/form-data">

			<div><br>
				<label >Codigo</label>
				<input type="text" name="codigo" ><br><br>
				<label >Nombre</label>
				<input type="text" name="nombre" ><br><br>
			</div>
			<div><br>
				<label >Apellido</label>
				<input type="text" name="apellido"><br><br>
				<label >Asig. Servicio</label>
				<select  name="service"> 
					<?php
						foreach (listarServicio($conn) as $key => $value ) {
					?>
					<option value="<?=$value[0]?>"><?=$value[1]?></option>
					<?php
						}
					?>
				</select>
			</div>
			<div><br>
				<label >Foto</label>
				<input type="file" name="photo" required><br><br>
				<input type="submit" name="aceptar" value="Aceptar">
			</div><br>
			
			
			
			<input type="hidden" name="accion" value="agregar">

			
			
		</form>

</body>

</html>
