<!DOCTYPE html>
<html>
<head>
	<title></title>
	
</head>
<body>

	<?php
		session_start();

		if(!isset($_SESSION['usuario']))
		header("location:../LoginAdmin.php");
		
		require('../../controlador/conexion.php');
		$conn=conectar();
	?>
	<h1>Listar Tecnicos</h1>
	<div>
		<table>
			<tr>
				<th>Codigo</th>
				<th>Nombre</th>
				<th>Apellido</th>
				<th>Foto</th>
				<th>Servicio</th>
				<th>Accion</th>
			</tr>
			<?php
				foreach (listarTecnico($conn) as $key => $value) {
			?>
			<tr>
				<td><?=$value[0]?></td>
				<td><?=$value[1]?></td>
				<td><?=$value[2]?></td>
				<td><img src="../../imagenes/<?=$value[4]?>"width="100" height="100"></td>
				<td><?=$value[3]?></td>
				<td>
					<a href="../../llamadas/procesoTecnico.php?accion=eliminar&codigo=<?=$value[0]?>">Eliminar</a>
					<a href="editar.php?codigo=<?=$value[0]?>">Modificar</a>
				</td>
			</tr>
			<?php
				}
				?>
			</table>
			
	</div>
		
</body>
		
</html>