<!DOCTYPE html>
<html>
<head>
	<title></title>
<link rel="stylesheet" type="text/css" href="../../css/miestilo.css">
	<link href="https://fonts.googleapis.com/css2?family=Rajdhani&display=swap" rel="stylesheet">
</head>

<body>
	<?php

		require('../../controlador/conexion.php');
		$conn = conectar();
		$cod = $_REQUEST['codigo'];
		$data = buscarTecnico($cod,$conn);
		$sertec=$data[2];
		$ser = buscarServicio($sertec,$conn);
	?>
	<h1>Editar Tecnicos</h1>
	<form action="../../llamadas/procesoTecnico.php" method="post" enctype="multipart/form-data">
		<input type="hidden" name="codigo" value="<?=$cod?>"><br>
		<div>

			<label>Nombre</label>
			<input type="text" name="nombre" value="<?=$data[0]?>"><br>
		</div>
		<div>
			<label>Apellido</label>
			<input type="text" name="apellido" value="<?=$data[1]?>"><br>
		</div>
		<div>
			<img src="../../images/nosotros/<?=$data[3]?>" width="100" heigth="100">
		</div>
		<div>
			<label>Foto </label>
			
			<input type="file" name="photo" required><br>
		</div>
		<div>
			<label>Elige Servicio</label>
			<select  name="service"> 
			<option value="<?=$data[2]?>"><?=$ser[0]?></option>
			<?php
				foreach (listarServicio($conn) as $key => $value ) {
			?>
			<option value="<?=$value[0]?>"><?=$value[1]?></option>

			<?php
				}
			?>
			</select>
			
			
		</div>
		<div>
			<input type="submit" name="actualizar" value="Actualizar">
			<input type="hidden" name="accion" value="modificar">
			
		</div>		
		
		
	</form>
</body>
</html>