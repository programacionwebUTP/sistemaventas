<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="../css/miestilo.css">
	<link href="https://fonts.googleapis.com/css2?family=Rajdhani&display=swap" rel="stylesheet">
    <script src="../js/login.js"></script>
</head>
<body>
<?php

include("../includes/header.php");
?>
		<br>
		<form action="../llamadas/procesoLoginUser.php" class="for-log centrado" name="formLogin">
			<fieldset class="centrado">
               
                <legend>Iniciar Sesion</legend>
                <h2 class="centrado">Login Usuario</h2>
                <label>Usuario:</label><br>
                <input type="text" name="user" placeholder="Ingrese su usuario" class="field"><br><br>
                <label>Password:</label><br>
                <input type="password" name="clave" placeholder="***" class="field"><br><br>
                <input type="submit" name="entrar" value="Entrar" class="field" ><br><br>
                 <a href="../paginas/usuario/agregar.php" >Registrarse</a>
            </fieldset>
	
		</form>

        <?php

include("../includes/footer.php");
?>
</body>
</html>