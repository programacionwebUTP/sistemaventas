<!DOCTYPE html>
<html>
<head>
    <title></title>
    <link rel="stylesheet" type="text/css" href="../css/Productos.css">
    <div class="conte_parent" id="cont_parent"></div>
</head>

<fieldset class="centrado">CAMARAS DE VIGILANCIA</fieldset>

<div class="conte">
    <div class="card equipos">
        <img src="../imagenes/vig1.jpg" width="300" >
    </div>
        <div class="info">
            <p>Cámara Interior DAIRU Wifi 1080P</p>
        </div>
            <div class="CPrecio">
                <div class="precio">
                    <span class="precio1"><b>S/149.00</b></span> 
                </div>
            </div>      
</div>


<div class="conte">
    <div class="card equipos">
        <img src="../imagenes/vig2.jpg" width="300" >
    </div>
        <div class="info">
            <p>Cámara de Exterior DAIRU Wifi 1080P</p>
        </div>
            <div class="CPrecio">
                <div class="precio">
                    <span class="precio1"><b>S/199.00</b></span> 
                </div>
            </div>      
</div>

<div class="conte">
    <div class="card equipos">
        <img src="../imagenes/vig4.jpg" width="300" >
    </div>
        <div class="info">
            <p>Cámaras de Seguridad Kit 10 Full HD 2TB WD + Exterior Mayor Alcance Varifocal40M</p>
        </div>
            <div class="CPrecio">
                <div class="precio">
                    <span class="precio1"><b>S/3,039.00</b></span> 
                </div>
            </div>      
</div>

<fieldset class="centrado">KITS DE VIGILANCIA</fieldset>

<div class="conte">
    <div class="card equipos">
        <img src="../imagenes/vig3.jpg" width="300" >
    </div>
        <div class="info">
            <p>Kit 9 Cámaras de Seguridad Full HD 1TB P2P + Kit Microfono + Cable Utp</p>
        </div>
            <div class="CPrecio">
                <div class="precio">
                    <center>
                    <span class="precio1"><b>  S/1,609.00</b></span>   
                    </center>
                </div>
            </div>      
</div>

<div class="conte">
    <div class="card equipos">
        <img src="../imagenes/cer1.jpg" width="300" >
    </div>
        <div class="info">
            <p>Cerradura Digital Push Pull SHS -P718</p>
        </div>
            <div class="CPrecio">
                <div class="precio">
                    <span class="precio1"><b>S/3,549.00</b></span> 
                </div>
            </div>      
</div>

<div class="conte">
    <div class="card equipos">
        <img src="../imagenes/cer2.jpg" width="300" >
    </div>
        <div class="info">
            <p>Cerradura Digital Ydm3212 - Yale</p>
        </div>
            <div class="CPrecio">
                <div class="precio">
                    <span class="precio1"><b>S/1,349.90</b></span> 
                </div>
            </div>      
</div>


<fieldset class="centrado">CERRADURAS ELECTRONICAS</fieldset>

<div class="conte">
    <div class="card equipos">
        <img src="../imagenes/dron1.jpg" width="300" >
    </div>
        <div class="info">
            <p>DJI Mini 2 Fly More Combo (NA)</p>
        </div>
            <div class="CPrecio">
                <div class="precio">
                    <span class="precio1"><b>S/2,899.00</b></span> 
                </div>
            </div>      
</div>
<fieldset class="centrado">DRONES CON SEGUIMIENTO</fieldset>

<div class="conte">
    <div class="card equipos">
        <img src="../imagenes/dron2.jpg" width="300" >
    </div>
        <div class="info">
            <p>Dron Mavic 3 Fly More Combo</p>
        </div>
            <div class="CPrecio">
                <div class="precio">
                    <span class="precio1"><b>S/15,999.00</b></span> 
                </div>
            </div>      
</div>

<br>
<center>
<a style="font-size: 30px" href="Productos.php">REGRESAR A PRODUCTOS</a>
</center>
</body>
</html>