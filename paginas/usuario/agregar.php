<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="../../css/miestilo.css">
	<link href="https://fonts.googleapis.com/css2?family=Rajdhani&display=swap" rel="stylesheet">
	<script type="text/javascript" src="../../js/registro.js"></script>
</head>
<body>
<?php

require('../../controlador/conexion.php');
include("../../includes/header.php");
$conn=conectar();

?>

		<br>
	<form  name="registro" action="../../llamadas/procesoRegistro.php" method="post">
			<fieldset class="centrado" >
                <legend>Registro de Usuario</legend>
                <h1>Cree su cuenta</h1>
				<label>Nombre de usuario:</label><br>
				<input type="text" name="nomeuser" placeholder="Juan..." class="field" id="nom"><br>
	
				<label>Contraseña:</label><br>
				<input type="password" name="pass" placeholder="****" class="field" id="passw"><br>
				<label>Nombre completo:</label><br>
				<input type="text" name="nomCom"  class="field" id="nomCom"><br>
				<div>
				<input type="hidden" name="accion" value="agregar">
				<input type="submit" name="aceptar" value="Aceptar">
				</div>

			</fieldset>
		</form>

        <?php

include("../../includes/footer.php");
?>
</body>
</html>