<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="../../css/miestilo.css">
	<link href="https://fonts.googleapis.com/css2?family=Rajdhani&display=swap" rel="stylesheet">
	<script type="text/javascript" src="../../js/registro.js"></script>
</head>
	<body>
		<?php
		session_start();
			if(!isset($_SESSION['usuario']))
				header("location:../LoginAdmin.php");

	

			require('../../controlador/conexion.php');
			$conn=conectar();
	?>
	<h2>Listar Usuario</h2>

	<div>
		<table>
			<tr>
				
				<th>Nombre Usuario</th>
				<th>Contraseña</th>
				<th>Nombre completo</th>
				<th>Accion</th>
			</tr>
				<?php
					foreach (listarUsuario($conn) as $key => $value) {
				?>
				<tr>
					
					<td><?=$value[0]?></td>
					<td><?=$value[1]?></td>
					<td><?=$value[2]?></td>
					<td>
						<nav>
							<a href="../../llamadas/procesoUsuario.php?accion=eliminar&codigo=<?=$value[0]?>">Eliminar</a>
							<a href="editar.php?codigo=<?=$value[0]?>">Modificar</a>
						</nav>
					</td>
				</tr>
				<?php
					}
					?>
			</table>		
	</div>
        <?php

include("../../includes/footer.php");
?>
</body>
</html>