<!DOCTYPE html>
<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="../../css/miestilo.css">
	<link href="https://fonts.googleapis.com/css2?family=Rajdhani&display=swap" rel="stylesheet">
	<script type="text/javascript" src="../../js/registro.js"></script>
</head>
<body>

	<?php
	session_start();
	if(!isset($_SESSION['usuario']))
			header("location:../loginAdm.php");
		
		require('../../controlador/conexion.php');
		$conn = conectar();
		$cod = $_REQUEST['codigo'];
		$data = buscarUsuario($cod,$conn);
	?>

		<h2>Editar Usuario</h2>
	<form action="../../llamadas/procesoUsuario.php" method="post">
		<!-- <input type="hidden" name="codigo" value="<?=$cod?>"><br> -->
		<label>Nombre</label>
		<input type="text" name="nombre" value="<?=$data[0]?>"><br>
		<label>Contraseña</label>
		<input type="password" name="pass" value="<?=$data[1]?>"><br>
		<label>Nombre completo</label>
		<input type="text" name="nomComp" value="<?=$data[2]?>"><br>
		<input type="hidden" name="accion" value="Actualizar">
		<input type="submit" name="actualizar" value="Actualizar">
	</form>

        <?php

include("../../includes/footer.php");
?>
</body>
</html>